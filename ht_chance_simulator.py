#!/bin/env python

import datetime
import seaborn as sns
import random
import statistics
from collections import Counter
import matplotlib.pyplot as plt

#scriptstart
scriptstart = datetime.datetime.now().time()
print("script started:\t",scriptstart,"\n")


#Define variables for chances distribution
nrOfCommonChances = 5
nrOfHomeChances = 5
nrOfAwayChances = 5

#variables for the simulation
nrOfSimulations = 500000

#ratings for about 67%-33% real probabilities
home_midfield = 10000
away_midfield = 7900
factor = 3.0


## --- EFFORT vars --- ##
# Define variables for effort idea
effortIncreasingCreateSharedChance = 0.07
effortIncreasingCreateExclusiveChance = 0.05
effortIncreasingDestroyExclusiveChance = 0.03

# Define new global variables for the ME.
mddHomeMFRealProb = 0.67
mddAwayMFRealProb = 0.33


## --- PERSONAL customization --- ## TODO - migrate to parsing arguments

nrOfSimulations = 1000
do_memory_less = True
do_simple_memory = True
do_effort = True


print("possession, home:\t\t",(home_midfield / (home_midfield + away_midfield)))
print("real probability, home:\t",(home_midfield**factor / (home_midfield**factor + away_midfield**factor)))


## --- SIMPLE memory-less --- ##
#Get if home is attacking
def homeGotIt(memory = 0):
  return random.random() <= (home_midfield**factor / (home_midfield**factor + away_midfield**factor) - memory)



## --- STATIC memory-effect --- ##
#Define function to calculate static memory as match dynamics
def getMemoryFactor(homeChances, awayChances):
  if homeChances - awayChances >= 2:
    return 0.05 * (homeChances - (awayChances + 1))
  elif awayChances - homeChances >= 2:
    return 0.05 * (awayChances - (homeChances + 1))
  else:
    return 0



## --- SCALED memory effect EFFORT --- ##
# Returns home probability to win the normal chance
def mddHomeWon(homeRealProb):
  return random.random() <= homeRealProb



# Returns a new normal chance: 1 = Shared, 2 = Home, 3 - Away
def mddCreateNormalChance(shared, exclusiveHome, exclusiveAway):
  chanceType = random.random()

  if chanceType <= 0.33:
    if shared < nrOfCommonChances:
      chanceType = 1
    else:
      if chanceType <= 0.16:
        if exclusiveHome < nrOfHomeChances:
          chanceType = 2
        else:
          chanceType = 3
      else:
        if exclusiveAway < nrOfAwayChances:
          chanceType = 3
        else:
          chanceType = 2
  else:
    if chanceType <= 0.66:
      if exclusiveHome < nrOfHomeChances:
        chanceType = 2
      else:
        if exclusiveAway < nrOfAwayChances:
          chanceType = 3
        else:
          chanceType = 1   
    else:
      if exclusiveAway < nrOfAwayChances:
        chanceType = 3
      else:
        if exclusiveHome < nrOfHomeChances:
          chanceType = 2
        else:
          chanceType = 1   
  return chanceType



## --- CHANCE DISTRIBUTION functions --- ##
#Simulate the chances from a single match
def runMatch():
  homeChances = 0
  awayChances = 0
  for i in range(nrOfHomeChances):
    if homeGotIt():
      homeChances += 1
  for i in range(nrOfAwayChances):
    if not homeGotIt():
      awayChances += 1
  for i in range(nrOfCommonChances):
    if homeGotIt():
      homeChances += 1
    else:
      awayChances += 1
  return [homeChances, awayChances, homeChances - awayChances]

#TODO - test randomly pulled chances instead of sequential home -> away -> shared
#Simulate the chances from a single match with a simple, static Memory
def runMatchWithMemory():
  homeChances = 0
  awayChances = 0
  memory = 0
  for i in range(nrOfHomeChances):
    memory = getMemoryFactor(homeChances, awayChances)
    if homeGotIt(memory):
      homeChances += 1
  for i in range(nrOfAwayChances):
    memory = getMemoryFactor(homeChances, awayChances)
    if homeGotIt(memory):
      awayChances += 1
  for i in range(nrOfCommonChances):
    memory = getMemoryFactor(homeChances, awayChances)
    if homeGotIt(memory):
      homeChances += 1
    else:
      awayChances += 1
  return [homeChances, awayChances, homeChances - awayChances]

# Simulate the chances from a single match using Effort
def runMatchWithEffort():

  # Chance distribution
  homeChances = 0
  awayChances = 0

  # Control for each chance type
  shared = 0
  exclusiveHome = 0
  exclusiveAway = 0

  # Total chances in a match
  totalChances = nrOfHomeChances + nrOfAwayChances + nrOfCommonChances

  # Effort starting level.
  effortLevelHome = 0.01
  effortLevelAway = 0.01

  # The cycles
  for i in range(totalChances):

    # Update Effort effect.
    effortRatioHome = effortLevelHome / (effortLevelHome + effortLevelAway)
    effortRatioAway = 1 - effortRatioHome
    effortEffectHome = (effortLevelHome - 0.01) * effortRatioHome
    effortEffectAway = (effortLevelAway - 0.01) * effortRatioAway

    # Apply effort effect over normal chance real probabilities.
    # At this point the REAL ME would use the actual ratings instead
    # of the constants "mddHomeMFRealProb" and "mddAwayMFRealProb"
    homeRealProb = mddHomeMFRealProb - effortEffectHome + effortEffectAway
    awayRealProb = mddAwayMFRealProb - effortEffectAway + effortEffectHome
    
    # Create a normal chance.
    chanceType = mddCreateNormalChance(shared, exclusiveHome, exclusiveAway)

    # Shared chance!
    if chanceType == 1:
      shared += 1
      effortIncreasing = effortIncreasingCreateSharedChance
      if mddHomeWon(homeRealProb):
        homeChances += 1
        effortLevelHome += effortIncreasing * (1 - homeRealProb)
      else:
        awayChances += 1
        effortLevelAway += effortIncreasing * (1 - awayRealProb)
    else:
      # Exclusive home!
      if chanceType == 2:
        exclusiveHome += 1
        if mddHomeWon(homeRealProb):
          homeChances += 1
          effortIncreasing = effortIncreasingCreateExclusiveChance
          effortLevelHome += effortIncreasing * (1 - homeRealProb)
        else:
          effortIncreasing = effortIncreasingDestroyExclusiveChance
          effortLevelAway += effortIncreasing * (1 - awayRealProb)
      # Exclusive Away!
      else:
        exclusiveAway += 1
        if mddHomeWon(homeRealProb):
          effortIncreasing = effortIncreasingDestroyExclusiveChance
          effortLevelHome += effortIncreasing * (1 - homeRealProb)
        else:
          awayChances += 1
          effortIncreasing = effortIncreasingCreateExclusiveChance
          effortLevelAway += effortIncreasing * (1 - awayRealProb)

  return [homeChances, awayChances, homeChances - awayChances]



# frequency function
def CountFrequency(my_list): 
  freq = {}
  for item in my_list: 
    if (item in freq): 
      freq[item] += 1
    else: 
      freq[item] = 1
  for key, value in sorted(freq.items()): 
    print ("% d : % d"%(key, value)) 





#Run the simulations with and without memory
simulation = [ runMatch() for x in range(nrOfSimulations) ]
simulationWithMemory = [ runMatchWithMemory() for x in range(nrOfSimulations) ]
simulationWithEffort = [ runMatchWithEffort() for x in range(nrOfSimulations) ]



if do_memory_less and do_simple_memory:
  print("********** MEMORY-LESS AND STATIC MEMORY **********\n")
  print("********** HOME CHANCES ***************************")

  sns.displot(data={'normal':list(zip(*simulation))[0], 'memory':list(zip(*simulationWithMemory))[0], 'effort':list(zip(*simulationWithEffort))[0}, kind="kde")
  print("-" * 30)
  print('Standard Deviation:', statistics.stdev(list(zip(*simulation))[0]))
  CountFrequency(list(zip(*simulation))[0])
  print('-' * 30)
  print('Standard Deviation with Memory:', statistics.stdev(list(zip(*simulationWithMemory))[0]))
  CountFrequency(list(zip(*simulationWithMemory))[0])
  print('-' * 30)
  print('Standard Deviation with Effort:', statistics.stdev(list(zip(*simulationWithEffort))[0]))
  CountFrequency(list(zip(*simulationWithEffort))[0])

  print("********* STATS FOR AWAY CHANCES *********")
  sns.displot(data={'normal':list(zip(*simulation))[1], 'memory':list(zip(*simulationWithMemory))[1], 'effort':list(zip(*simulationWithEffort))[1]}, kind="kde")
  print("-" * 30)
  print('Standard Deviation:', statistics.stdev(list(zip(*simulation))[1]))
  CountFrequency(list(zip(*simulation))[1])
  print("-" * 30)
  print('Standard Deviation with Memory:', statistics.stdev(list(zip(*simulationWithMemory))[1]))
  CountFrequency(list(zip(*simulationWithMemory))[1])
  print("-" * 30)
  print('Standard Deviation with Effort:', statistics.stdev(list(zip(*simulationWithEffort))[1]))
  CountFrequency(list(zip(*simulationWithEffort))[1])

  print("********* STATS FOR HOME - AWAY CHANCES *********")
  sns.displot(data={'normal':list(zip(*simulation))[2], 'memory':list(zip(*simulationWithMemory))[2], 'effort':list(zip(*simulationWithEffort))[2]}, kind="kde")
  print("-" * 30)
  print('Standard Deviation:', statistics.stdev(list(zip(*simulation))[2]))
  CountFrequency(list(zip(*simulation))[2])
  print("-" * 30)
  print('Standard Deviation with Memory:', statistics.stdev(list(zip(*simulationWithMemory))[2]))
  CountFrequency(list(zip(*simulationWithMemory))[2])
  print("-" * 30)
  print('Standard Deviation with Effort:', statistics.stdev(list(zip(*simulationWithEffort))[2]))
  CountFrequency(list(zip(*simulationWithEffort))[2])

"""
if do_memory_less and do_effort:
  print("EFFORT ===> ********* STATS FOR HOME CHANCES *********")
  sns.displot(data={'normal':list(zip(*simulation))[0], 'effort':list(zip(*simulationWithEffort))[0]}, kind="kde")
  print("*" * 30)
  print('Standard Deviation:', statistics.stdev(list(zip(*simulation))[0]))
  CountFrequency(list(zip(*simulation))[0])
  print('-' * 15)
  print('Standard Deviation with Effort:', statistics.stdev(list(zip(*simulationWithEffort))[0]))

  print("EFFORT ===> ********* STATS FOR AWAY CHANCES *********")
  sns.displot(data={'normal':list(zip(*simulation))[1], 'effort':list(zip(*simulationWithEffort))[1]}, kind="kde")
  print("*" * 30)
  print('Standard Deviation:', statistics.stdev(list(zip(*simulation))[1]))
  CountFrequency(list(zip(*simulation))[1])
  print('-' * 15)
  print('Standard Deviation with Effort:', statistics.stdev(list(zip(*simulationWithEffort))[1]))
  CountFrequency(list(zip(*simulationWithEffort))[1])

  print("EFFORT ===> ********* STATS FOR HOME - AWAY CHANCES *********")
  sns.displot(data={'normal':list(zip(*simulation))[2], 'effort':list(zip(*simulationWithEffort))[2]}, kind="kde")
  print("*" * 30)
  print('Standard Deviation:', statistics.stdev(list(zip(*simulation))[2]))
  CountFrequency(list(zip(*simulation))[2])
  print('-' * 15)
  print('Standard Deviation with Effort:', statistics.stdev(list(zip(*simulationWithEffort))[2]))
  CountFrequency(list(zip(*simulationWithEffort))[2])


if do_simple_memory and do_effort:
  print("EFFORT ===> ********* STATS FOR HOME CHANCES *********")
  sns.displot(data={'memory':list(zip(*simulationWithMemory))[0], 'effort':list(zip(*simulationWithEffort))[0]}, kind="kde")
  print("*" * 30)
  print('Standard Deviation with Memory:', statistics.stdev(list(zip(*simulationWithMemory))[0]))
  CountFrequency(list(zip(*simulationWithMemory))[0])
  print('-' * 15)
  print('Standard Deviation with Effort:', statistics.stdev(list(zip(*simulationWithEffort))[0]))

  print("EFFORT ===> ********* STATS FOR AWAY CHANCES *********")
  sns.displot(data={'memory':list(zip(*simulationWithMemory))[1], 'effort':list(zip(*simulationWithEffort))[1]}, kind="kde")
  print("*" * 30)
  print('Standard Deviation with Memory:', statistics.stdev(list(zip(*simulationWithMemory))[1]))
  CountFrequency(list(zip(*simulationWithMemory))[1])
  print('-' * 15)
  print('Standard Deviation with Effort:', statistics.stdev(list(zip(*simulationWithEffort))[1]))
  CountFrequency(list(zip(*simulationWithEffort))[1])

  print("EFFORT ===> ********* STATS FOR HOME - AWAY CHANCES *********")
  sns.displot(data={'memory':list(zip(*simulationWithMemory))[2], 'effort':list(zip(*simulationWithEffort))[2]}, kind="kde")
  print("*" * 30)
  print('Standard Deviation with Memory:', statistics.stdev(list(zip(*simulationWithMemory))[2]))
  CountFrequency(list(zip(*simulationWithMemory))[2])
  print('-' * 15)
  print('Standard Deviation with Effort:', statistics.stdev(list(zip(*simulationWithEffort))[2]))
  CountFrequency(list(zip(*simulationWithEffort))[2])
"""


#scriptend
scriptend = datetime.datetime.now().time()
print("\nscript finished:\t",scriptend)
time_elapsed = ( scriptend.second - scriptstart.second ) + ( ( scriptend.minute - scriptstart.minute ) * 60 ) + ( ( scriptend.hour - scriptstart.hour ) * 3600 )
print("\ntime elapsed:\t",time_elapsed," seconds")

