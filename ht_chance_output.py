#!/usr/bin/python
# -*- coding: utf-8 -*-

#
# --------------------------------------------------------------------------------------------------
# "THE BEER-WARE LICENSE" (Revision 42):
# <thomas.rosted@protonmail.com> wrote this file, based on work of HT-Tasos. 
# Credit, ownership, and other stuff, and also beer, should be directed in his general direction.
# I claim no ownership or anything else, but I have not received feedback regarding potential
# license information, and have chosen this license for arbitrary reasons.
#
# As long as you retain this notice, do whatever you want with this script. If we meet some day,
# and you think that this script is worth it, feel free to buy me a beer or whatever else you think
# is appropriate, it would be a welcome but unexpected surprise.
# - Thomas Rosted Larsen
#
# Credit to Poul-Henning Kamp for this license, whom I will probably buy a beer or whatever.
# --------------------------------------------------------------------------------------------------
#
# This script handles most output to console for png_name_generator.py
# written and tested on Ubuntu Disco using Python 3.7.3
#

# version
version_text = '\n{name} version {version}\n'

# help text
help_text  = '\n'

help_text += "  Description:\n\n"
help_text += "    This script runs a number of simulations using different\n"
help_text += "    parameters to demonstrate ways to control variance.\n\n"

help_text += "  Context settings:\n\n"
help_text += "    --show-details or --hide-details:\n"
help_text += "      BOOLEAN value, default False, toggles detailed output\n"
help_text += "    --verbose or --compact:\n"
help_text += "      same as --show-details or --hide-details\n\n"

help_text += "    --show-graphs:        BOOLEAN value, default False\n"
help_text += "    --hide-graphs:        Show or hide graphs\n\n"

help_text += "    -g or --global:       Default local, toggles scope of future\n"
help_text += "    -l or --local:        parameters. Useful for fine-tuning.\n\n"

help_text += "    -h or --help:         print help text (this) and exit\n\n"

help_text += "    --version:            print version information and exit\n\n"

help_text += "    --list or --dry-run:  BOOLEAN value, default False\n"
help_text += "      do not run simulations, but print setup to console and exit\n\n"

help_text += "    --debug:              BOOLEAN value, default False\n"
help_text += "                          Enables debug mode\n\n"

help_text += "  Parameters:\n\n"
help_text += "    -n or --simulations:\n"
help_text += "      INTEGER, number of simulation repetitions, default 100,000\n"
help_text += "      This is a context parameter and can be set only once\n\n"

help_text += "    -s or -t or --simulation-type:\n"
help_text += "      INTEGER, represents the code for the simulation type\n"
help_text += "      Current available simulation types are:\n"
help_text += "        1: 'memoryLess' - unmodified simulations (default)\n"
help_text += "        2: 'staticNorm' - static modified midfield\n"
help_text += "        3: 'staticRand' - static random manipulation\n"
help_text += "        4: 'effortMech' - dynamic modified midfield\n"
help_text += "      The name as string value can be passed as well, but this\n"
help_text += "      is discouraged for complexity reasons. It might improve\n"
help_text += "      readability in input files and is included for that reason\n\n"

help_text += "    -a or --home-midfield:\n"
help_text += "      INTEGER value, default 10,000\n"
help_text += "      Adjust the home team midfield value\n\n"

help_text += "    -b or --away-midfield:\n"
help_text += "      INTEGER value, default 10,000\n"
help_text += "      Adjusts the away team midfield value\n\n"

help_text += "    -r or --relative-midfield:\n"
help_text += "      INTEGER value, no default value, must be between 0 and 100\n"
help_text += "      Overrides the midfield ratio for the relevant simulation(s)\n\n"

help_text += "    -p or --exp-power:\n"
help_text += "      FLOATING POINT value, default 3.0\n"
help_text += "      Adjust the exponential weight of midfield values\n\n"

help_text += "    -c or --chance-distro:\n"
help_text += "      SET of 3 values, default {5,5,5}\n"
help_text += "      Adjust the composition of HOME, AWAY, SHARED chances\n\n"

help_text += "  File I/O:\n\n"
help_text += "    -i or --input:\n"
help_text += "      BOOLEAN value, default False, toggles input from file\n"
help_text += "      Default source: /current/working/dir/htcs_input.csv\n"
help_text += "      Replace by supplying file target using --file\n\n"

help_text += "    -o or --output:\n"
help_text += "      BOOLEAN value, default False, toggles output to file\n"
help_text += "      Default destination: /current/working/dir/htcs_output.csv\n"
help_text += "      Replace by supplying file target using --file\n\n"

help_text += "    -f or --file:\n"
help_text += "      /path/to/file.name, no default value\n"
help_text += "      MUST follow either -i --input or -o --output\n"
help_text += "      Uses relative path from current working directory\n"
help_text += "      Can be used twice for both source and destination files\n"
help_text += "      Note: if multiple input files specified, all but the final\n"
help_text += "      context will be discarded. Context parameters set via\n"
help_text += "      cmd arguments will always override parameters from file\n\n"

help_text += "  Usage Examples:\n\n"
help_text += "    Shorthand for running the script\n"
help_text += "      ~(run) = python /path/to/ht-chance-simulator.py\n\n"
help_text += "    Context parameters:\n\n"
help_text += "      simulate memoryless x500 and show details:\n"
help_text += "        ~(run) --show-details -nt 500 1\n\n"
help_text += "      the same but long notation:\n"
help_text += "        ~(run) --show-details --simulations --simulation-type 500 1\n\n"
help_text += "      simulate memoryless and staticnorm x500 with power 4 and graphs:\n"
help_text += "        ~(run) -ntptp --show-graphs 500 1 4 2 4\n\n"

help_text += "    Simulation parameters:\n\n"
help_text += "      simulate memoryless and staticnorm with 55.9% possession:\n"
help_text += "        ~(run) -tab 1 10000 7900 -tab 2 10000 7900\n\n"
help_text += "      simulate effortmech with 50%, 56% and 67% possession\n"
help_text += "        ~(run) -tab 4 10000 10000 -tab 4 10000 7900 -tab 4 10000 4800\n\n"

help_text += "    File I/O:\n\n"
help_text += "      Read from default inputfile\n"
help_text += "        cd /path/to/script; ~(run) -i\n"
help_text += "          OR\n"
help_text += "        ~(run) -if /path/to/inputfile\n"
help_text += "      write to default outputfile, created in cwd if not exists\n"
help_text += "        ~(run) -o (or --output)\n\n"
help_text += "      write to file /path/to/myfile.txt\n"
help_text += "        ~(run) -of /path/to/myfile.txt\n\n"


# generic
period_newline  = '.\n'
script_start = '\n  script started:\t{_start}\n'
script_end = '\n  script finished:\t{_stop}'
script_duration = '  time elapsed:\t{_duration} seconds\n'

# user feedback
argument_mismatch = '  something went wrong, mismatch between arguments and parameters supplied\n'
introduce_dump = '  information dump:\n'
notify_debug_arg = '  for more information, add --debug and repeat the call\n  this will dump the arg stack to console, allowing manual identification of the flawed argument(s)'
dryrun_complete = '  dry-run completed'
unknown_simulation_type = '  simulation type {_sim} was not recognized, using default memoryLess'
unknown_long_arg_01 = '  argument not found:\t{_arg}\n  accepted arguments:\n'
unknown_long_arg_02 = '\n  see help text for details\n'

# writing to file
create_file_success = '  succesfully created file {name}'
create_file_failure = '  failed to create file {name}'
write_success = '\n  {count} lines succesfully written to file {filename}'
write_failure = '  error writing to file'
permission_error = '  failed to create parent directories'

# file validation
missing_source = '  - no source file supplied, using default'
source_accepted = '  source file {_src} accepted'
source_replaced = '  source file {_src} rejected, using default {_def}'
source_rejected = '  source file {_src} and default source file {_def} were both rejected, simulation halted'
missing_destination = '  - no destination supplied, using default'
destination_accepted = '  destination {dest} accepted'
destination_rejected = '  destination {dest} rejected, using default'

# context presentation
context_details_term = '  Context: {count} simulations to console.\n'
context_details_file = '  Context: {count} simulations to {filename}.\n'
context_details_verbose = '  Detailed information is {status},'
context_details_graphs  = '{_and}graphs will{_not}be displayed'
context_details_debug   = ', and debug mode is enabled'

# simulation description
simulation_details  = '  Simulation added: {simtype} with {simhome} home midfield, {simaway} away midfield,\n'
simulation_details += '  exponential power {simfactor} and {simdistro} chance distribution.'



# STANDARD INFORMATION
def print_version(vname,vversion):
  print(version_text.format(name=vname,version=vversion))

def print_help_text():
  print(help_text)

def print_script_begin(start):
  print(script_start.format(_start=start))

def print_script_end(stop):
  print(script_end.format(_stop=stop))

def print_script_duration(duration):
  print(script_duration.format(_duration=duration))

def print_dryrun_complete():
  print(dryrun_complete)


# SIMULATION SETTINGS
def print_simulation_details(simulation):
  distro = ""
  for sector in simulation['chance_distro']: distro += str(sector) + '+'
  return simulation_details.format(simtype=simulation['simulation_type'],simhome=simulation['home_midfield'],simaway=simulation['away_midfield'],simfactor=simulation['exp_power'],simdistro=distro[:-1])


# CONTEXT SETTINGS
def print_context_details(context):
  if not context['output'] == 'file': to_print = context_details_term.format(count=context['simulation_count'])
  else: to_print = context_details_file.format(count=context['simulation_count'],filename=context['output_file'])

  if context['show_details']: to_print += context_details_verbose.format(status='enabled')
  else: to_print += context_details_verbose.format(status='disabled')

  if context['debug_mode']: dbg_and = ' '
  else: dbg_and = ' and '
  if context['show_graphs']: dbg_not = ' '
  else: dbg_not = ' not '
  to_print += context_details_graphs.format(_and=dbg_and,_not=dbg_not)

  if context['debug_mode']: to_print += context_details_debug
  to_print += period_newline
  print(to_print)


# FILE HANDLING
def print_write_success(_filename, _count):
  print(write_success.format(filename=_filename, count=_count))

def print_write_failure():
  print(write_failure)

def print_create_file_success(filename):
  print(create_file_success.format(name=filename))

def print_create_file_failure(filename):
  print(create_file_failure.format(name=filename))


# FILE VALIDATION
def print_no_source():
  print(missing_source)

def print_source_accepted(_source):
  print(source_accepted.format(_src=_source))

def print_source_replaced(_source, _default):
  print(source_replaced.format(_src=_source,_def=_default))

def print_source_rejected(_source, _default):
  print(source_rejected.format(_src=_source,_def=_default))

def print_no_destination():
  print(missing_destination)

def print_destination_accepted(destination):
  to_print = destination_accepted.format(dest=destination)
  return to_print

def print_destination_rejected(destination):
  print(destination_rejected.format(dest=destination))

def print_permission_error():
  print(permission_error)


# INPUT VALIDATION
def print_argument_mismatch(arg_stack, _debug):
  print(argument_mismatch)
  if _debug: print(introduce_dump); print(arg_stack); print()
  else: print(notify_debug_arg)

def print_unknown_simulation_type(sim):
  print(unknown_simulation_type.format(_sim=sim) + "\n")

def print_unknown_long_arg(arg, *args):
  to_print = unknown_long_arg_01
  for _arg in args: to_print += '    ' + _arg + '\n'
  to_print += unknown_long_arg_02
  print(to_print.format(_arg=arg))


# DEBUG INFORMATION
def print_arg_stack(args, processed_args, sim_params, expected_args, _context, _simulations):
  to_print  = "args:\t" + str(args)
  to_print += "\nprocessed args:\t" + str(processed_args)
  to_print += "\narg parameters:\t" + str(sim_params)
  to_print += "\nexpected args:\t" + str(expected_args)
  return to_print

