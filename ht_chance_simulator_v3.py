#!/bin/env python

import datetime
import seaborn as sns
import random
import statistics
from collections import Counter
import matplotlib.pyplot as plt

#
# ----------------------------------------------------------------------------
# "THE BEER-WARE LICENSE" (Revision 42):
# <thomas.rosted@protonmail.com> wrote this file, based on work of HT-Tasos. 
# Credit, ownership, and other stuff, and also beer, should be directed in
# his general direction. I claim no ownership or anything else, but have not
# received feedback regarding potential license information, hence this one.
#
# As long as you retain this notice, do whatever you want with this script.
# If we meet some day, and you think the script is worth it, you can buy me
# a beer in return.
# - Thomas Rosted Larsen
#
# Credit to Poul-Henning Kamp, who I will probably buy a beer whenever.
# ----------------------------------------------------------------------------




### --- SPECIALIZED CHANCE DISTRIBUTIONS BEGIN --- ###
###
### this section contains one function for each possible chance
### distribution variation. helper functions nested if unique to
### the current variation, or listed above in the area above, 
### dedicated to helper functionality
###
### run_without_memory() is a pure, fully independent implementation
### run_static_normal() uses a static modifier to possession
### run_static_random() alters the range of the
###   random input deciding chance success
### run_dynamic_effort() uses a dynamic possession modifier
###

# simulate chances without memory
def run_without_memory(home_success = 0, away_success = 0):

  def assign_chance_without_memory(home_midfield, away_midfield, factor):
    return random.random() <= (home_midfield**factor / (home_midfield**factor + away_midfield**factor))

  home_midfield, away_midfield, factor = sim_object.get_match_params()
  for i in range(home_chances):
    if assign_chance_without_memory(home_midfield, away_midfield, factor): home_success += 1
    if not assign_chance_without_memory(home_midfield, away_midfield, factor): away_success += 1
    if assign_chance_without_memory(home_midfield, away_midfield, factor): home_success += 1
    else: away_success += 1
  return [home_success, away_success, home_success - away_success]

# simulate chances by using a static modifier to possession
def run_static_normal(home_success = 0, away_success = 0, memory = 0):

  def assign_chance_static_normal(home_midfield, away_midfield, factor, memory = 0):
    return random.random() <= ((home_midfield**factor / (home_midfield**factor + away_midfield**factor)) - memory)

  def get_memory_effect(home_success, away_success):
    if home_success > away_success: return 0.05 * (home_success - away_success - 1)
    if away_success > home_success: return 0.05 * (home_success - away_success + 1)
    return 0

  home_midfield, away_midfield, factor = sim_object.get_match_params()
  chance_array = sectors * 5
  random.shuffle(chance_array)
  for i, domain in enumerate(chance_array):
    memory = get_memory_effect(home_success, away_success)
    outcome = assign_chance_static_normal(home_midfield, away_midfield, factor, memory)
    if outcome and domain != "AWAY": home_success+=1
    elif not outcome and domain != "HOME": away_success+=1
  return [home_success, away_success, home_success - away_success]

# simulate chances by manipulating range of the random function
def run_static_random(home_success = 0, away_success = 0, memory = 0):

  def assign_chance_static_random():
    if home_success > away_success:
      return (random.randrange(0 + (50 * (home_success - away_success - 1 )), 1000) / 1000) <= (home_midfield**factor / (home_midfield**factor + away_midfield**factor))
    if home_success < away_success:
      return (random.randrange(0, 1000 - (50 * (away_success - home_success - 1))) / 1000) <= (home_midfield**factor / (home_midfield**factor + away_midfield**factor))
    return (random.uniform(0, 1000) / 1000) <= (home_midfield**factor / (home_midfield**factor + away_midfield**factor))

  home_midfield, away_midfield, factor = sim_object.get_match_params()
  chance_array = sectors * 5
  random.shuffle(chance_array)
  for i, domain in enumerate(chance_array):
    outcome = assign_chance_static_random()
    if outcome and domain != "AWAY": home_success+=1
    elif not outcome and domain != "HOME": away_success+=1
  return [home_success, away_success, home_success - away_success]

# Simulate the chances from a single match using Effort
def run_dynamic_effort(home_success = 0, away_success = 0):

  def assign_chance_dynamic_effort(adjusted_odds):
    return random.random() <= adjusted_odds

  def update_effort(outcome, domain):
    if outcome and domain == "HOME":
      return home_effort_level + (create_exclusive_cost * (1 - adjusted_odds)), away_effort_level
    if not outcome and domain == "HOME":
      return home_effort_level, away_effort_level + (destroy_exclusive_cost * adjusted_odds)
    if outcome and domain == "AWAY":
      return home_effort_level + (destroy_exclusive_cost * (1 - adjusted_odds)), away_effort_level
    if not outcome and domain == "AWAY":
      return home_effort_level, away_effort_level + (create_exclusive_cost * adjusted_odds)
    if outcome and domain == "SHARED":
      return home_effort_level + (create_shared_cost * (1 - adjusted_odds)), away_effort_level
    if not outcome and domain == "SHARED":
      return home_effort_level, away_effort_level + (create_shared_cost * adjusted_odds)

  home_midfield, away_midfield, factor = sim_object.get_match_params()
  # effort constants
  create_shared_cost = 0.07
  create_exclusive_cost = 0.05
  destroy_exclusive_cost = 0.03
  # effort variables
  home_effort_level = 0.01
  away_effort_level = 0.01
  # chance array, 5+5+5
  chance_array = sectors * 5
  random.shuffle(chance_array)
  for i, domain in enumerate(chance_array):
    home_effort_effect = (home_effort_level - 0.01) * (home_effort_level / (home_effort_level + away_effort_level))
    away_effort_effect = (away_effort_level - 0.01) * (1 - (home_effort_level / (home_effort_level + away_effort_level)))
    real_ratio = ((home_midfield**factor) / (home_midfield**factor + away_midfield**factor))
    adjusted_odds = real_ratio - home_effort_effect + away_effort_effect
    outcome = assign_chance_dynamic_effort(adjusted_odds)
    if outcome and domain != "AWAY": home_success+=1
    elif not outcome and domain != "HOME": away_success+=1
    home_effort_level, away_effort_level = update_effort(outcome, domain)
  return [home_success, away_success, home_success - away_success]


### --- SPECIALIZED CHANCE DISTRIBUTIONS END --- ###




### --- SIMULATIONS BEGIN --- ###
###
### main functions for running the simulations, statistical
### analysis and output to the user
###
### make_simtypes() runs the chosen simulations
### plot_chance_stats() plots the results
### print_chance_stats() prints the results
### count_frequency() counts the frequency
###
### better explanations require better understanding
### of the matplotlib and seaborn modules
### TODO - learn matplotlib and seaborn
###

# run the chosen simulations
def make_simtypes():
  print("\npreparing", sim_object.sim_count, "simulations...")
  if "currentME" in simlist:
    print("simulating current ME...")
    without_memory_simulation = [ run_without_memory() for x in range(sim_object.sim_count) ]
    simtypes["IndependentME"] = without_memory_simulation
  if "staticNorm" in simlist:
    print("simulating static modifier...")
    static_normal_simulation = [ run_static_normal() for x in range(sim_object.sim_count) ]
    simtypes["staticMemoryMod"] = static_normal_simulation
  if "randRange" in simlist:
    print("simulating modified randomRange...")
    static_random_simulation = [ run_static_random() for x in range(sim_object.sim_count) ]
    simtypes["randomRangeMod"] = static_random_simulation
  if "useEffort" in simlist:
    print("simulating dynamic effort...")
    dynamic_effort_simulation = [ run_dynamic_effort() for x in range(sim_object.sim_count) ]
    simtypes["dynamicEffort"] = dynamic_effort_simulation

# plot the results
def plot_chance_stats():
  result_array = []
  for i, sector in enumerate(sectors):
    print("plotting", sector, "chances")
    result_array.append(dict())
    for simtype in simtypes:
      print("result_array[i]:", result_array[i])
      print("result_array[i][simtype]:", result_array[i][simtype])
      print("simtypes[simtype]:", simtypes[simtype])
      print("*simtypes[simtype]:", *simtypes[simtype])
      print("list(zip(*simtypes[simtype]))[i]:", list(zip(*simtypes[simtype]))[i])
      result_array[i][simtype] = list(zip(*simtypes[simtype]))[i]
    sns.displot(data=result_array[i], kind="kde")

# print the results
def print_chance_stats():
  sectors[2] = "DIFF, HOME"
  for i, sector in enumerate(sectors):
    print("\n**********", sector, "CHANCES ***************************")
    for j, simtype in enumerate(simtypes):
      print()
      print(simtype,'mean:\t\t\t', statistics.mean(list(zip(*simtypes[simtype]))[i]))
#      print(simtype,'variance:\t\t', statistics.variance(list(zip(*simtypes[simtype]))[i]))
      print(simtype,'standard deviation:\t', statistics.stdev(list(zip(*simtypes[simtype]))[i]))
      if show_details: count_frequency(list(zip(*simtypes[simtype]))[i])
  if show_plots: plt.show()

# frequency counter
def count_frequency(my_list): 
  freq = {}
  for item in my_list: 
    if (item in freq): freq[item] += 1
    else: freq[item] = 1
  for key, value in sorted(freq.items()): 
    print ("% d : % d"%(key, value)) 

### --- SIMULATIONS END --- ###




### --- DEBUG TOOLS BEGIN --- ###
###
### some functions purely for debugging
###

def get_custom_values():
  sim_count = 1000000
  without_memory = True
  static_normal = False
  static_random = False
  dynamic_effort = False
  home_midfield = 10000
  away_midfield = 4800
  factor = 3.0
  return sim_count, without_memory, dynamic_effort, static_normal, static_random, home_midfield, away_midfield, factor

# playground - set debug_mode to True to skip regular simulations and jump here
def go_off_the_trail():
#  do_stuff()
  make_simtypes()
  plot_chance_stats()
  print_chance_stats()
#  do_more_stuff()
  for i in range(6):
    sim_object.set_factor(sim_object.factor + 0.5)
    make_simtypes()
    plot_chance_stats()
    print_chance_stats()


### --- DEBUG TOOLS END --- ###



### --- INITIALIZATION BEGIN --- ###

# simulations
class sim_object:
  sim_count = 50000
  without_memory = True
  static_normal = True
  static_random = True
  dynamic_effort = True
  home_midfield = 10000
  away_midfield = 7900
  factor = 3.0

#  def __init__(sim_count, without_memory, dynamic_effort, static_normal, static_random, home_midfield, away_midfield, factor):
  def __init__(self):
    if custom_values:
      sim_count, without_memory, dynamic_effort, static_normal, static_random, home_midfield, away_midfield, factor = get_custom_values()
      self.sim_count = sim_count
      self.without_memory = without_memory
      self.static_normal = static_normal
      self.static_random = static_random
      self.dynamic_effort = dynamic_effort
      self.home_midfield = home_midfield
      self.away_midfield = away_midfield
      self.factor = factor

  def get_match_params(self):
    return self.home_midfield, self.away_midfield, self.factor
  def set_factor(self, factor):
    self.factor = factor


# to add new types of simulations, create a bool here
# to skip simulations, set the relevant bool to False

# constants
shared_chances = 5
home_chances = 5
away_chances = 5

#ratings for about 67%-33% real probabilities
home_midfield = 10000
away_midfield = 7900
factor = 3.0

# debugging
custom_values = True
show_plots = False
show_details = True
debug_mode = False

sectors = ["HOME","AWAY","SHARED"]
simlist = []
simtypes = {}
sim_object = sim_object()

def init_values():
  init_simlist()
  home_mid, away_mid, _factor = sim_object.get_match_params()
  print("possession, home:\t", format(((100 * home_mid) / (home_mid + away_mid)),'.1f'),"%")
  print("real probability, home:\t",format(((100 * home_mid**_factor) / (home_mid**_factor + away_mid**_factor)),'.1f'),"%\n")
  if debug_mode:
    go_off_the_trail()
    return False
  return True

def init_simlist():
  if sim_object.without_memory: simlist.append("currentME")
  if sim_object.static_normal: simlist.append("staticNorm")
  if sim_object.static_random: simlist.append("randRange")
  if sim_object.dynamic_effort: simlist.append("useEffort")

def parse_args():
  pass

### --- INITIALIZATION END --- ###

# here we go
if __name__ == '__main__':
  scriptstart = datetime.datetime.now().time()
  print("\nscript started:\t",scriptstart,"\n")
  parse_args()
  init_values()
  make_simtypes()
  plot_chance_stats()
  print_chance_stats()

  scriptend = datetime.datetime.now().time()
  print("\nscript finished:\t",scriptend)
  time_elapsed = ( scriptend.second - scriptstart.second ) + ( ( scriptend.minute - scriptstart.minute ) * 60 ) + ( ( scriptend.hour - scriptstart.hour ) * 3600 )
  print("\ntime elapsed:\t",time_elapsed," seconds")

