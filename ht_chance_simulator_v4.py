#!/bin/env python

import sys, os, random, statistics, time
import ht_chance_output as output
import seaborn as sns
import matplotlib.pyplot as plt

#
# --------------------------------------------------------------------------------------------------
# "THE BEER-WARE LICENSE" (Revision 42):
# <thomas.rosted@protonmail.com> wrote this file, based on work of HT-Tasos. 
# Credit, ownership, and other stuff, and also beer, should be directed in his general direction.
# I claim no ownership or anything else, but I have not received feedback regarding potential
# license information, and have chosen this license for arbitrary reasons.
#
# As long as you retain this notice, do whatever you want with this script. If we meet some day,
# and you think that this script is worth it, feel free to buy me a beer or whatever else you think
# is appropriate, it would be a welcome but unexpected surprise.
# - Thomas Rosted Larsen
#
# Credit to Poul-Henning Kamp for this license, whom I will probably buy a beer or whatever.
# --------------------------------------------------------------------------------------------------

NAME = 'ht-chance-simulator'
VERSION = 0.4.0.1
SIMULATIONS = []
CONTEXT = {}
SIM_DATA = []
RESULTS = []
DEBUG_ARRAY = []
INSTANCE = {}
SECTORS = ["HOME","AWAY","SHARED"]
INSTANCE['script_started'] = time.time()



### --- SPECIALIZED CHANCE DISTRIBUTIONS BEGIN --- ###
### this section contains one function for each possible chance distribution variation.
### helper functions nested if unique to the current variation.
### additionally, a generic call function run_simulation() can return any of the above.
###
### run_without_memory() is a pure, fully independent implementation
### run_static_normal() uses a static modifier to possession
### run_static_random() alters the range of the random input deciding chance outcome
### run_dynamic_effort() uses a dynamic possession modifier calculated from the team ratio
### TODO - consider un-nesting helper functions and merge where applicable
### TODO - implement Motivation
### TODO - implement async simulations, should drastically reduce runtime

### helper function, purpose is to have a generic call that refers to the proper simulation function
def run_simulation(sim):
  if sim['simulation_type'] == 'memoryLess': return run_without_memory(sim)
  if sim['simulation_type'] == 'staticNorm': return run_static_normal(sim)
  if sim['simulation_type'] == 'staticRand': return run_static_random(sim)
  if sim['simulation_type'] == 'effortMech': return run_dynamic_effort(sim)

### standard simulation function, returns the distribution of chances without memory
def run_without_memory(sim, home_success = 0, away_success = 0):

  # helper function - returns True / False if the random value beats the odds
  def assign_chance_without_memory(home_midfield, away_midfield, factor):
    return random.random() <= (home_midfield**factor / (home_midfield**factor + away_midfield**factor))

  home_midfield, away_midfield, factor, chance_array = get_match_params(sim)
  for chance in chance_array:
    outcome = assign_chance_without_memory(home_midfield, away_midfield, factor)
    if outcome and chance != "AWAY": home_success+=1
    elif not outcome and chance != "HOME": away_success+=1
  return [home_success, away_success, home_success - away_success]

### simulation function, returns the distribution of chances using staticNorm
### achieved by manipulating the core probability to achieve a memory effect
def run_static_normal(sim, home_success = 0, away_success = 0, memory = 0):

  # helper function - returns True / False if the random value beats the odds
  def assign_chance_static_normal(home_midfield, away_midfield, factor, memory = 0):
    return random.random() <= ((home_midfield**factor / (home_midfield**factor + away_midfield**factor)) - memory)

  def get_memory_effect(home_success, away_success):
    if home_success > away_success: return 0.05 * (home_success - away_success - 1)
    if away_success > home_success: return 0.05 * (home_success - away_success + 1)
    return 0

  home_midfield, away_midfield, factor, chance_array = get_match_params(sim)
  for chance in chance_array:
    memory = get_memory_effect(home_success, away_success)
    outcome = assign_chance_static_normal(home_midfield, away_midfield, factor, memory)
    if outcome and chance != "AWAY": home_success+=1
    elif not outcome and chance != "HOME": away_success+=1
  return [home_success, away_success, home_success - away_success]

### simulation function, returns the distribution of chances using staticRand
### achieved by manipulating the range of the random function as opposed to the core probability
def run_static_random(sim, home_success = 0, away_success = 0, memory = 0):

  # helper function - returns True / False if the random value beats the odds - modified
  def assign_chance_static_random(home_midfield, away_midfield, factor):
    if home_success > away_success:
      return (random.randrange(0 + (50 * (home_success - away_success - 1 )), 1000) / 1000) <= (home_midfield**factor / (home_midfield**factor + away_midfield**factor))
    if home_success < away_success:
      return (random.randrange(0, 1000 - (50 * (away_success - home_success - 1))) / 1000) <= (home_midfield**factor / (home_midfield**factor + away_midfield**factor))
    return (random.uniform(0, 1000) / 1000) <= (home_midfield**factor / (home_midfield**factor + away_midfield**factor))

  home_midfield, away_midfield, factor, chance_array = get_match_params(sim)
  for chance in chance_array:
    outcome = assign_chance_static_random(home_midfield, away_midfield, factor)
    if outcome and chance != "AWAY": home_success+=1
    elif not outcome and chance != "HOME": away_success+=1
  return [home_success, away_success, home_success - away_success]

### simulation function, returns the distribution of chances using effortMech
### achieved by continuously updating a variable for each team, varying by ratio and action
def run_dynamic_effort(sim, home_success = 0, away_success = 0):

  # helper function - returns True / False if the random value beats the odds
  def assign_chance_dynamic_effort(adjusted_odds):
    return random.random() <= adjusted_odds

  # helper function - updates the effort-meter
  def update_effort(outcome, domain):
    if outcome and domain == "HOME": return home_effort_level + (create_exclusive_cost * (1 - adjusted_odds)), away_effort_level
    if not outcome and domain == "HOME": return home_effort_level, away_effort_level + (destroy_exclusive_cost * adjusted_odds)
    if outcome and domain == "AWAY": return home_effort_level + (destroy_exclusive_cost * (1 - adjusted_odds)), away_effort_level
    if not outcome and domain == "AWAY": return home_effort_level, away_effort_level + (create_exclusive_cost * adjusted_odds)
    if outcome and domain == "SHARED": return home_effort_level + (create_shared_cost * (1 - adjusted_odds)), away_effort_level
    if not outcome and domain == "SHARED": return home_effort_level, away_effort_level + (create_shared_cost * adjusted_odds)

  home_midfield, away_midfield, factor, chance_array = get_match_params(sim)
  # effort constants
  create_shared_cost = 0.07
  create_exclusive_cost = 0.05
  destroy_exclusive_cost = 0.03
  # effort variables
  home_effort_level = 0.01
  away_effort_level = 0.01

  for chance in chance_array:
    home_effort_effect = (home_effort_level - 0.01) * (home_effort_level / (home_effort_level + away_effort_level))
    away_effort_effect = (away_effort_level - 0.01) * (1 - (home_effort_level / (home_effort_level + away_effort_level)))
    real_ratio = ((home_midfield**factor) / (home_midfield**factor + away_midfield**factor))
    adjusted_odds = real_ratio - home_effort_effect + away_effort_effect
    outcome = assign_chance_dynamic_effort(adjusted_odds)
    if outcome and chance != "AWAY": home_success+=1
    elif not outcome and chance != "HOME": away_success+=1
    home_effort_level, away_effort_level = update_effort(outcome, chance)
  return [home_success, away_success, home_success - away_success]

### helper function, returns simulation details
def get_match_params(sim):
  chance_array = []
  for i, c in enumerate(sim['chance_distro']):
    for j in range(int(c)):
      chance_array.append(SECTORS[i])
  random.shuffle(chance_array)
  return int(sim['home_midfield']), int(sim['away_midfield']), float(sim['exp_power']), chance_array

### --- SPECIALIZED CHANCE DISTRIBUTIONS END --- ###



### --- SIMULATIONS BEGIN --- ###
### main functions for running the simulations, statistical analysis, and output to console or file.
###
### make_datasets() runs the simulations
### plot_chance_stats() plots the data
### print_chance_stats() prints the data to console
### write_file() writes the data to file

### this function runs the chosen simulations
def make_datasets():
  global CONTEXT, SIMULATIONS, SIM_DATA
  print("  preparing", CONTEXT['simulation_count'], "simulations.\n")
  # iterate the simulations
  for sim in SIMULATIONS:
    print("  simulating", sim['simulation_type'])
    # lookup simulation_count n, and run the designated simulation that many times
    # each simulation returns 3 integers - home goals, away goals and home_diff (home goals minus away goals)
    # the results array of each simulation is stored in a global results array
    SIM_DATA.append([run_simulation(sim) for x in range(int(CONTEXT['simulation_count']))])

### this function is responsible for organizing the results
def plot_chance_stats():
  global SIM_DATA, SIMULATIONS, SECTORS
  named_sims = []
  # sort by category - home / away / diff_home
  for i, sector in enumerate(SECTORS):
    # note: prints SHARED chances - inaccurate but more user-friendly
    print("\n  plotting", sector, "chances", end='')
    RESULTS.append(dict())
    # sort by simulation
    for j, dataset in enumerate(SIM_DATA):
      # simple header - the incremented numeric value and the simulation type
      sim_name = str(j+1) + ':' + SIMULATIONS[j]['simulation_type']
      named_sims.append(sim_name)
      # the full dataset is appended to the RESULTS array-of-dicts
      # the first array is the category, and the nested dict key,value is header,data
      RESULTS[i][sim_name] = list(zip(*dataset))[i]
      print('.', end='');
    # finally, plot the data for later
    if CONTEXT['show_graphs']: sns.displot(data=RESULTS[i], kind="kde")
  print()

### this function is responsible for printing the simulation results to terminal
def print_chance_stats():
  global CONTEXT
  # quick repurpose of the sector array for home/away/shared - now home/away/diff_home
  SECTORS[2] = "DIFF, HOME"
  # sort by category - home / away / diff_home
  for i, sector in enumerate(SECTORS):
    print("\n  **********", SECTORS[i], "CHANCES ***************************")
    # sort by simulation
    for result in RESULTS[i]:
      print("\n  ",result,'mean:\t\t\t', statistics.mean(RESULTS[i][result]))
      print("  ",result,'standard deviation:\t', statistics.stdev(RESULTS[i][result]))
      # if verbose - add simulation breakdown
      if CONTEXT['show_details']:
        freq = {}
        # count each unique result
        for item in RESULTS[i][result]: 
          if (item in freq): freq[item] += 1
          else: freq[item] = 1
        # print simulation details
        for key, value in sorted(freq.items()): 
          print ("    % d :\t% d"%(key, value))
  if CONTEXT['show_graphs']: plt.show()

### this function is responsible for writing the simulation results to file
def write_file():
  dest = CONTEXT["output_file"]
  # quick repurpose of the sector array for home/away/shared - now home/away/diff_home
  SECTORS[2] = "DIFF_HOME"
  to_write = []
  # sort by category - home / away / diff_home
  for i, sector in enumerate(SECTORS):
    # create result-string-array header
    to_write.append("{sector}_CHANCES".format(sector=str(SECTORS[i])))
    # sort by simulation
    for result in RESULTS[i]:
      _result = RESULTS[i][result]
      # format and append mean and standard deviation strings to the result-string-array
      to_write.append("{_sim}_mean,{_mean}".format(_sim=str(result),_mean=str(statistics.mean(_result))))
      to_write.append("{_sim}_stdev,{_sd}".format(_sim=str(result),_sd=str(statistics.stdev(_result))))
      # if verbose - add simulation breakdown
      if CONTEXT['show_details']:
        freq = {}
        # count each unique result
        for item in _result:
          if (item in freq): freq[item] += 1
          else: freq[item] = 1
        # format and append the details to the result-string-array
        for key, value in sorted(freq.items()):
          to_write.append("{_key},{_value}".format(_key=str(key),_value=str(value)))
  # ttw - time-to-write
  with open(CONTEXT["output_file"], 'w') as data:
    try:
      for count, line in enumerate(to_write):
        data.writelines(line+'\n')
      output.print_write_success(CONTEXT["output_file"], count+1)
    except OSError: output.print_write_failure()

### --- SIMULATIONS END --- ###


### --- INITIALIZATION BEGIN --- ###
### this section only holds one function, parse_args()
### in extension, nested functions make up a large part of the functionality.
### ranging from validation, parsing, casting and verification to reading from file.
### TODO - consider un-nesting the nested functions for improved usability if imported
### TODO - guesstimate the chance distro if fewer than 3 ints are passed
### TODO - include this in expected_values
### TODO - implement --global and --local
### TODO - implement relative midfield

### this function validates arguments, decides the context and assembles the recipes for simulations
def parse_args(args):

  # --- NESTED FUNCTIONS BEGIN --- #

  # this function validates if the passed input file exists, and reads it line by line
  # input files must follow a specific format, with relevant data as comma-separated-values
  # additionally, python comments # are used to comment out lines as well as to signal new sections
  def read_input_file(infile = 'htcs_input.csv'):
    filename = parse_filepath('htcs_input.csv')
    _context = {}
    _simulations = []
    # input file accepted
    if os.path.exists(parse_filepath(infile)): output.print_source_accepted(infile); filename = parse_filepath(infile)
    # input file rejected, default input file used
    elif os.path.exists(filename): output.print_source_replaced(infile, filename)
    # both the submitted and the default input file were rejected
    else: output.print_source_rejected(infile, filename); return _context, _simulations
    with open(filename, 'r') as data:
      context_done = False
      try:
        for line in data.read().splitlines():
          # update the control variable and create an empty dict for the next simulation
          if line.startswith('#') and line.split(' ')[1] == 'sim':
            context_done = True
            _simulations.append(dict())
          # skip blank lines and python comments in the csv file - NOTE: saves the continue command in the IF statement above
          if len(line) == 0 or line.startswith('#'): continue
          # remainder of lines should be standard csv format, where index 0 is the dict key and the remaining fields are values
          fields = line.split(',')                                          # split into array by comma
          if not context_done: _context[fields[0]] = fields[1]; continue    # each context key has only one value
          elif len(fields) < 3: _simulations[-1][fields[0]] = fields[1]     # multiple simulations must be handled
          else: _simulations[-1][fields[0]] = [ *fields[1:] ]               # special rule for keys with array of values
      except OSError: output.print_read_failure()
    return _context, _simulations

  # this function finalizes the context, and auto-completes omitted parameters with default values
  # non-string values are parsed to avoid type issues down the line
  def add_context(_context):
    global CONTEXT
    # default values to be overriden if context parameters are passed
    context = {'simulation_count':1000, 'output':'console', 'output_file':'htcs_output.csv' ,'show_details':False, 'show_graphs':False, 'debug_mode':False}
    if 'simulation_count' in _context: context['simulation_count'] = validate_field(_context['simulation_count'], 'int')
    if 'show_details' in _context: context['show_details'] = parse_bool(_context['show_details'])
    if 'output_file' in _context: context['output_file'] = _context['output_file']
    if 'show_graphs' in _context: context['show_graphs'] = parse_bool(_context['show_graphs'])
    if 'debug_mode' in _context: context['debug_mode'] = parse_bool(_context['debug_mode'])
    if 'output' in _context: context['output'] = _context['output']
    # set the global context and flip some booleans
    CONTEXT = context
    # feedback to the user is formatted and printed immediately
    output.print_context_details(context)

  # this function finalizes a simulation, and auto-completes omitted parameters with default values
  # non-string values are parsed to avoid type issues down the line
  def add_simulation(_simulation):
    global SIMULATIONS
    # default values to be overriden if simulation parameters are passed
    simulation = {'home_midfield':10000, 'away_midfield':10000, 'exp_power':3.0, 'simulation_type':'currentME' ,'chance_distro':[5,5,5]}
    if 'simulation_type' in _simulation: simulation['simulation_type'] = get_simulation_type(_simulation['simulation_type'])
    if 'home_midfield' in _simulation: simulation['home_midfield'] = validate_field(_simulation['home_midfield'], 'int')
    if 'away_midfield' in _simulation: simulation['away_midfield'] = validate_field(_simulation['away_midfield'], 'int')
    if 'chance_distro' in _simulation: simulation['chance_distro'] = get_chance_distro(_simulation['chance_distro'])
    if 'exp_power' in _simulation: simulation['exp_power'] = validate_field(_simulation['exp_power'], 'float')
    # add the simulation to the global simulations list
    SIMULATIONS.append(simulation)
    # feedback to user is formatted and stored for later
    return output.print_simulation_details(simulation)

  # this function tests the simulation type, and returns it in string format
  # it accepts both string format and integer values
  def get_simulation_type(simtype):
    if simtype in simulation_types: return simtype
    if parse_int(simtype):
      if int(simtype) == 1: return 'memoryLess'
      if int(simtype) == 2: return 'staticNorm'
      if int(simtype) == 3: return 'staticRand'
      if int(simtype) == 4: return 'effortMech'
    output.print_unknown_simulation_type(simtype)
    return get_simulation_type(1)

  # this function validates and returns a proper list of integers - hopefully
  def get_chance_distro(args):
    distro_array = []
    # hardcoded limitation to 3 sectors - home / away / shared
    # TODO      implement guesstimating the setup
    #           for example, 2 values ignore shared and a single value ignores exclusive
    if len(args) > 3: return False
    for arg in args:
      # if arg can be converted to integer, append it as integer
      if parse_int(arg): distro_array.append(parse_int(arg))
      else: distro_array.append(0)
    # hardcoded expansion to 3 sectors - home / away / shared
    while len(distro_array) < 3: distro_array.append(0)
    return distro_array

  # this function validates the filename supplied, if it exists, creates it if not, creates directories if needed, etc
  def verify_filepath(_filename, dry_run):
    # filename must contain some sort of file type # hashtag windows-rules
    if len(_filename.split('.')) < 2: return False
    # use helper funtion to join filename with current working directory
    filename = parse_filepath(_filename)
    # reject dash as first character in filename
    if filename.startswith('-'): return False
    # short filenames rejected
    if (len(filename) < 3) or (len(os.path.basename(filename)) < 3): return False
    # if dry-run, don't create missing files or directories
    if dry_run and os.path.exists(filename): return True
    # check if parent directory is a directory
    if not os.path.isdir(os.path.dirname(filename)):
      # create necessary directories supplied by the user
      try: os.makedirs(os.path.dirname(filename))
      except (OSError): output.print_permission_error(); return False
    try:
      # test file by opening it, creating an empty file if it doesn't exist
      with open(filename, 'a'): pass
      output.print_create_file_success(filename)
      return True
    except (OSError):
      # user feedback if file doesn't exist and cannot be created
      output.print_create_file_failure(filename)
      return False

  # helper function, properly casts variables if possible, or returns false
  # this allows a call like IF validate field THEN var = validate field
  # can probably be optimized to prevent the redundant call, but for now it
  # provides flexibility for iterating a list of varying variable types
  def validate_field(_field, _type):
    # if int or float is specified, cast and return the int/float else return false
    if _type == 'int' and parse_int(_field): return parse_int(_field)
    if _type == 'float' and parse_float(_field): return parse_float(_field)
    # test if the string value matches a boolean and return it if succesfull
    if str(_field).capitalize() == 'True': return True
    if str(_field).capitalize() == 'False': return False
    # not actually guessing, used for iterating over different types
    # booleans were handled above, so if it's not an int, it's a string
    # for now, there is no need to test for floating point values since only
    # context parameters are processed in here, hence integer or string types
    if _type == 'guess' and not parse_int(_field): return str(_field)
    else: return parse_int(_field)

  # some helper functions - self-explanatory
  def parse_filepath(_filename):
    return os.path.join(os.getcwd(), _filename)
  def parse_float(f):
    try: res = float(f); return res
    except ValueError: return False
  def parse_int(i):
    try: res = int(i); return res
    except ValueError: return False
  def parse_bool(b):
    if str(b).capitalize() == 'True': return True
    else: return False

  # --- NESTED FUNCTIONS END --- #

  # --- MANDATORY PARAMETERS BEGIN --- #

  # the following 4 lists are for lookup and validation
  # short_args and long_args are used for validating the arguments passed, discarding flawed arguments,
  # simulation_types is the current string representations of the different simulation types, and
  # parameter_list is a dictionary that pairs short notation with long notation
  short_args = 'htcs bootstrapping tool final'
  long_args = ['show-details','hide-details','verbose','compact','show-graphs','hide-graphs','input','output','dry-run','list','file','simulation-type','home-midfield','away-midfield','relative-midfield','simulations','exp-power','chance-distro','global','local','help','version','debug']
  simulation_types = ["","memoryLess","staticNorm","staticRand","effortMech"]
  parameter_list = {'s':'simulation_type','t':'simulation_type','a':'home_midfield','b':'away_midfield','p':'exp_power','c':'chance_distro','i':'input','o':'output','f':'file','n':'simulation_count'}
  # two arrays for keeping track of parameters and their respective values
  # note that their indices are unrelated, tracked by a control variable once ready for processing
  prepared_parameters = []
  parameter_values = []
  # the context defines the overall simulation settings such as number of repetitions and how to display the results
  # only a single context is needed, and redundant parameters will simply overwrite the existing parameter
  this_context = {}
  # this simulation dictionary will be updated with prepared parameters and their values during the second loop
  # this allows testing if a parameter is already present, and finalize + flush before continuing if it is
  this_simulation = {}
  # when reading input from file, store it for later
  # this allows the combination of reading most values from file, and add or overwrite specific parts
  simulations_from_file = []
  context_from_file = {}
  # since the context is finalized after all the simulations, this array will store the individual
  # simulation formatted description strings for later output
  simulation_descriptions = []
  # global args define the context of how simulation parameters are processed
  # not global means that any parameter is exclusive to the current simulation and will be forgotten once finalized
  global_args = False
  # a few control booleans for knowing when to exit
  print_version_info = False
  early_exit = False
  dry_run = False

  # --- MANDATORY PARAMETERS END --- #

  # --- ARGUMENT EVALUATION LOOP BEGIN --- #

  # this loop evaluates all arguments by testing if they are part of the short_args and long_args lists, discarding them if not
  # some boolean values can only be passed using long notation, these are applied directly
  # the remaining parameters are stored for later regardless of long or short notation
  # everything that is not preceded by a dash is regarded as a parameter value and are stored without being evaluated
  for i, arg in enumerate(args):
    # long notation
    if arg.startswith('--'):
      # ignore the argument if unknown
      if not arg[2:] in long_args: output.print_unknown_long_arg(arg[2:], *long_args); continue
      # context settings
      if arg[2:] == 'show-graphs': this_context['show_graphs'] = True
      if arg[2:] == 'hide-graphs': this_context['show_graphs'] = False
      if arg[2:] == 'show-details': this_context['show_details'] = True
      if arg[2:] == 'hide-details': this_context['show_details'] = False
      if arg[2:] == 'verbose': this_context['show_details'] = True
      if arg[2:] == 'compact': this_context['show_details'] = False
      if arg[2:] == 'global': prepared_parameters.append('g')
      if arg[2:] == 'local': prepared_parameters.append('l')
      if arg[2:] == 'help': prepared_parameters.append('h')
      if arg[2:] == 'version': print_version_info = True
      if arg[2:] == 'dry-run': dry_run = True
      if arg[2:] == 'list': dry_run = True
      if arg[2:] == 'debug': this_context['debug_mode'] = True; CONTEXT['debug_mode'] = True
      # parameters
      if arg[2:] == 'simulations': prepared_parameters.append('n')
      if arg[2:] == 'simulation-type': prepared_parameters.append('t')
      if arg[2:] == 'home-midfield': prepared_parameters.append('a')
      if arg[2:] == 'away-midfield': prepared_parameters.append('b')
      if arg[2:] == 'relative-midfield': prepared_parameters.append('r')
      if arg[2:] == 'exp-power': prepared_parameters.append('p')
      if arg[2:] == 'chance-distro': prepared_parameters.append('c')
      # file IO
      if arg[2:] == 'input': prepared_parameters.append('i')
      if arg[2:] == 'output': prepared_parameters.append('o')
      if arg[2:] == 'file': prepared_parameters.append('f')
      continue
    # short notation
    if len(arg) > 1 and arg.startswith('-'):
      for ch in arg[1:]:
        if ch in short_args: prepared_parameters.append(ch)
      continue
    # assume unhandled arg is a parameter value and store it for later without further scrutiny
    parameter_values.append(arg)

  # --- ARGUMENT EVALUATION LOOP END --- #

  # if --version --help or -h supplied, print and exit
  # this allows supplying both, as well as both long and short notation for help
  if print_version_info: output.print_version(NAME,VERSION); early_exit = True
  if 'h' in prepared_parameters: output.print_help_text(); early_exit = True
  if early_exit: CONTEXT['debug_mode'] = False; return False
  # expected args calculates how many values are supposed to be stored
  # a mismatch will notify the user and exit, to prevent index out-of-bounds
  # TODO    test for the presence of --chance-distro and adapt appropriately
  #         IF expected args minus 1 THEN assume no shared
  #         IF expected args minus 2 THEN assume shared only
  expected_args = len(prepared_parameters) + (prepared_parameters.count('c') * 2) - prepared_parameters.count('i') - prepared_parameters.count('o')
  arg_stack = output.print_arg_stack(args, prepared_parameters, parameter_values, expected_args, CONTEXT, SIMULATIONS)
  if not expected_args == len(parameter_values):
    try: output.print_argument_mismatch(arg_stack, this_context['debug_mode'])
    except KeyError: output.print_argument_mismatch(arg_stack, False); CONTEXT['debug_mode'] = False
    return False

  # --- ARGUMENT PROCESSING LOOP BEGIN --- #

  # control variable for parameters with values != 1
  j = 0
  # this loop iterates the processed arguments from earlier, and pairs them with their respective values
  # some tests are performed to avoid invalid values, but the original string value will be used regardless
  # this is because reading input from file also requires validation and parsing / casting, so actual conversion might as well be delayed
  for i, arg in enumerate(prepared_parameters):
    # finalize the current simulation if this arg already exists, and clear the simulation dict
    if parameter_list[arg] in this_simulation:
      simulation_descriptions.append(add_simulation(this_simulation))
      this_simulation.clear()
    # TODO - implement global + local control args
    # global and local are intended as keywords for simulation parameters, to prevent excessive typing
    # for example, if testing exp_power = 2.5, each simulation entered via cmdargs will otherwise require manually typing -p 2.5
    # adding the -g or --global keyword will register all future parameters as global, until -l or --local is registered
    # implementation unknown, but probably store i+j in an array, and process everything a second time upon completion
    if arg == 'g': global_args = True; j -= 1
    if arg == 'l': global_args = False; j -= 1
    # TODO - implement relative midfield
    # should not take long to override -ab but skip for now
    if arg == 'r': continue
    if arg == 's': this_simulation['simulation_type'] = parameter_values[i+j]
    if arg == 't': this_simulation['simulation_type'] = parameter_values[i+j]
    if arg == 'a' and parse_int(parameter_values[i+j]): this_simulation['home_midfield'] = parameter_values[i+j]
    if arg == 'b' and parse_int(parameter_values[i+j]): this_simulation['away_midfield'] = parameter_values[i+j]
    if arg == 'n' and parse_int(parameter_values[i+j]): this_context['simulation_count'] = parameter_values[i+j]
    if arg == 'p' and parse_float(parameter_values[i+j]): this_simulation['exp_power'] = parameter_values[i+j]
    if arg == 'c' and get_chance_distro(parameter_values[i+j:i+j+3]): this_simulation['chance_distro'] = parameter_values[i+j:i+j+3]; j += 2
    # simple test to determine whether or not this arg is the final arg
    more_args = len(prepared_parameters) > i + 1
    # if -i or --input was passed, test if the next arg specifies from-file
    if arg == 'i':
      j -= 1
      if more_args and prepared_parameters[i + 1] == 'f': pass
      else: output.print_no_source(); context_from_file, simulations_from_file = read_input_file()
    # if -o or --output was passed, test if the next arg specifies from-file
    if arg == 'o':
      this_context['output'] = 'file'; j -= 1
      if more_args and prepared_parameters[i + 1] == 'f': pass
      else: print(output.print_no_destination())
    # if -f or --file was passed, test if the previous arg was -i --input
    if arg == 'f' and prepared_parameters[i-1] == 'i':
      context_from_file, simulations_from_file = read_input_file(parameter_values[i+j])
    # if -f or --file was passed, test if the previous arg was input or output
    if arg == 'f' and prepared_parameters[i-1] == 'o':
      if verify_filepath(parameter_values[i+j], dry_run):
        this_context['output_file'] = parameter_values[i+j]
        print(output.print_destination_accepted(parameter_values[i+j]))
      else: print(output.print_destination_rejected(parameter_values[i+j]))
    # finalize current simulation if this was the last argument
    if not more_args:
      if len(this_simulation) > 0: simulation_descriptions.append(add_simulation(this_simulation))

  # --- ARGUMENT PROCESSING LOOP END --- #

  # if input was read from file, add the simulations
  # simulations added via cmd args are added first, but one does not exclude the other
  if len(simulations_from_file) > 0:
    for sim in simulations_from_file:
      simulation_descriptions.append(add_simulation(sim))
  # if context was read from file, use it instead of default values
  # context parameters defined via cmd args will overwrite those from the input file
  if len(context_from_file) > 0:
    for field in this_context:
      this_field = validate_field(this_context[field], 'guess')
      context_from_file[field] = this_field
    add_context(context_from_file)
  else: add_context(this_context)
  # the add_context() function above prints the formatted context description to console
  # since all individual simulations have already been finalized, they can now be printed as well
  for res in simulation_descriptions: print(res,"\n")
  # debug functionality works better when capitalized oO
  if CONTEXT['debug_mode']:
    global DEBUG_ARRAY
    DEBUG_ARRAY.append(arg_stack)
  # if --dry-run or --list arguments are passed, notify the user and return false to skip the actual simulations
  if dry_run: output.print_dryrun_complete(); CONTEXT['debug_mode'] = False; return False
  return True

### --- INITIALIZATION END --- ###


### --- PRIMARY SCRIPT --- ###
### this section has the main() function, and a run_debug() function for testing purposes
###
### the main() function forwards cmd args to the parse_args() function, and if succesfull,
### starts the simulation process. if the --debug argument was passed, the run_debug() function
### is injected between parse_args() and the simulate() nested function.
###
### the simulation process is confined to the nested simulate() function, that calls
### a) make_datasets()
### b) plot_chance_stats()
### c) print_chance_stats() OR write_file()
### depending on the processed arguments, the default being print_chance_stats()
###
### the run_debug() function prints every imaginable parameter immediately after arguments have
### been parsed, and the return boolean instructs the main function to skip simulations if false,
### or to proceed if true.
###
### feel free to alther this behavior, but be aware of the interaction between the main function
### and the run_debug return value.

### debug function - no designated behavior
def run_debug():
  # do_stuff()
  print(CONTEXT)
  print(SIMULATIONS)
  print(sys.argv)
  print([arg for arg in DEBUG_ARRAY])
  print()
  # do_more_stuff()
  return False # False for early exit && True for forcing simulations

### here we go
def main(args):
  # main function calls, nested for the ability to easier skip the actual simulations
  # also allows running multiple simulations concurrently
  def simulate():
    # make_datasets() runs the simulations and stores the results
    make_datasets()
    # plot_chance_stats() processes the results
    plot_chance_stats()
    # output function calls - prints to console or file depending on input - default is console
    if CONTEXT["output"] == 'file': write_file()
    else: print_chance_stats()
  output.print_script_begin(time.asctime(time.localtime(INSTANCE['script_started'])))
  # parse arguments and store the result boolean for later
  # if parse_args returns false, it can be from a mismatch between parameters and expected values,
  # from passing the --dry-run or --list arguments, or from an early exit argument --help or --version
  # the mismatch scenario properly notifies the user of the optional --debug argument
  do_simulations = parse_args(args)
  # if debug mode is enabled and the debug function instructs us to proceed, we proceed
  # the order is important, allowing the debug function to do it's thing before normal operations commence
  if CONTEXT['debug_mode'] and run_debug(): simulate()
  # we also proceed if arguments were succesfully parsed # hashtag business as usual
  elif do_simulations: simulate()
  # note time at end of script and print some time stuff
  INSTANCE['script_stopped'] = time.time()
  output.print_script_end(time.asctime(time.localtime(INSTANCE['script_stopped'])))
  output.print_script_duration(round(INSTANCE['script_stopped'] - INSTANCE['script_started'],2))

### boilerplate
if __name__ == '__main__':
  if len(sys.argv) > 1: main(sys.argv[1:])
  else: output.print_help_text()
  exit()

