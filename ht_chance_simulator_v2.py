#!/bin/env python

import datetime
import seaborn as sns
import random
import statistics
from collections import Counter
import matplotlib.pyplot as plt



#Define variables for chances distribution
nrOfCommonChances = 5
nrOfHomeChances = 5
nrOfAwayChances = 5

#variables for the simulation
nrOfSimulations = 500000

#ratings for about 67%-33% real probabilities
home_midfield = 10000
away_midfield = 7900
factor = 3.0


## --- EFFORT vars --- ##
# Define variables for effort idea
effortIncreasingCreateSharedChance = 0.07
effortIncreasingCreateExclusiveChance = 0.05
effortIncreasingDestroyExclusiveChance = 0.03

# Define new global variables for the ME.
mddHomeMFRealProb = 0.67
mddAwayMFRealProb = 0.33


## --- PERSONAL customization --- ## TODO - migrate to parsing arguments

nrOfSimulations = 10000
do_normal = True
do_effort = False
static_normal = False
static_random = False

sectors = ["HOME","AWAY","SHARED"]
images = []
simtypes = {}

print("possession, home:\t", format(((100 * home_midfield) / (home_midfield + away_midfield)),'.1f'),"%")
print("real probability, home:\t",format(((100 * home_midfield**factor) / (home_midfield**factor + away_midfield**factor)),'.1f'),"%\n")


## --- SIMPLE memory-less --- ##
#Get if home is attacking
def homeGotIt(memory = 0):
  if not live: home_midfield, away_midfield, factor = get_debug_vars()
  return random.random() <= (home_midfield**factor / (home_midfield**factor + away_midfield**factor) - memory)


## --- STATIC memory-effect --- ##
#Define function to calculate static memory as match dynamics
def getMemoryFactor(homeChances, awayChances):
  if homeChances - awayChances >= 2:
    return 0.05 * (homeChances - (awayChances + 1))
  elif awayChances - homeChances >= 2:
    return 0.05 * (awayChances - (homeChances + 1))
  else:
    return 0

# static memomy effect, manipulating the range of the coinflip
def getMemoryRandomValue(homeChances, awayChances):
  if not live: home_midfield, away_midfield, factor = get_debug_vars()
  if homeChances - awayChances >= 2:
    return (random.randrange(0 + (50 * (homeChances - awayChances - 1 )), 1000) / 1000) <= (home_midfield**factor / (home_midfield**factor + away_midfield**factor))
  elif awayChances - homeChances >= 2:
    return (random.randrange(0, 1000 - (50 * (awayChances - homeChances - 1))) / 1000) <= (home_midfield**factor / (home_midfield**factor + away_midfield**factor))
  else:
    return (random.uniform(0, 1000) / 1000) <= (home_midfield**factor / (home_midfield**factor + away_midfield**factor))


## --- SCALED memory effect EFFORT --- ##
# Returns home probability to win the normal chance
def mddHomeWon(homeRealProb):
  return random.random() <= homeRealProb



# Returns a new normal chance: 1 = Shared, 2 = Home, 3 - Away
def mddCreateNormalChance(shared, exclusiveHome, exclusiveAway):
  chanceType = random.random()

  if chanceType <= 0.33:
    if shared < nrOfCommonChances:
      chanceType = 1
    else:
      if chanceType <= 0.16:
        if exclusiveHome < nrOfHomeChances:
          chanceType = 2
        else:
          chanceType = 3
      else:
        if exclusiveAway < nrOfAwayChances:
          chanceType = 3
        else:
          chanceType = 2
  else:
    if chanceType <= 0.66:
      if exclusiveHome < nrOfHomeChances:
        chanceType = 2
      else:
        if exclusiveAway < nrOfAwayChances:
          chanceType = 3
        else:
          chanceType = 1   
    else:
      if exclusiveAway < nrOfAwayChances:
        chanceType = 3
      else:
        if exclusiveHome < nrOfHomeChances:
          chanceType = 2
        else:
          chanceType = 1   
  return chanceType



## --- CHANCE DISTRIBUTION functions --- ##
#Simulate the chances from a single match
def runMatch():
  homeChances = 0
  awayChances = 0
  for i in range(nrOfHomeChances):
    if homeGotIt():
      homeChances += 1
  for i in range(nrOfAwayChances):
    if not homeGotIt():
      awayChances += 1
  for i in range(nrOfCommonChances):
    if homeGotIt():
      homeChances += 1
    else:
      awayChances += 1
  return [homeChances, awayChances, homeChances - awayChances]


#Simulate the chances from a single match with a simple, static Memory
def runMatchWithMemory():
  homeSuccess = 0
  awaySuccess = 0
  memory = 0

  chanceArray = sectors * 5
  random.shuffle(chanceArray)

  for i, domain in enumerate(chanceArray):
    memory = getMemoryFactor(homeSuccess, awaySuccess)
    outcome = homeGotIt(memory)
    if outcome and domain != "AWAY":
      homeSuccess+=1
    elif not outcome and domain != "HOME":
      awaySuccess+=1
  return [homeSuccess, awaySuccess, homeSuccess - awaySuccess]


def runMatchWithRandomRangeMemory():
  homeSuccess = 0
  awaySuccess = 0
  memory = 0

  chanceArray = sectors * 5
  random.shuffle(chanceArray)

  for i, domain in enumerate(chanceArray):
    outcome = getMemoryRandomValue(homeSuccess, awaySuccess)
    if outcome and domain != "AWAY":
      homeSuccess+=1
    elif not outcome and domain != "HOME":
      awaySuccess+=1
  return [homeSuccess, awaySuccess, homeSuccess - awaySuccess]


# Simulate the chances from a single match using Effort
def runMatchWithEffort():
  if not live: home_midfield, away_midfield, factor = get_debug_vars()

  # Chance distribution
  homeChances = 0
  awayChances = 0

  # Control for each chance type
  shared = 0
  exclusiveHome = 0
  exclusiveAway = 0

  # Total chances in a match
  totalChances = nrOfHomeChances + nrOfAwayChances + nrOfCommonChances

  # Effort starting level.
  effortLevelHome = 0.01
  effortLevelAway = 0.01

  # The cycles
  for i in range(totalChances):
    # Update Effort effect.
    effortRatioHome = effortLevelHome / (effortLevelHome + effortLevelAway)
    effortRatioAway = 1 - effortRatioHome
    effortEffectHome = (effortLevelHome - 0.01) * effortRatioHome
    effortEffectAway = (effortLevelAway - 0.01) * effortRatioAway

    # Apply effort effect over normal chance real probabilities.
    # At this point the REAL ME would use the actual ratings instead
    # of the constants "mddHomeMFRealProb" and "mddAwayMFRealProb"
    real_ratio = ((home_midfield**factor) / (home_midfield**factor + away_midfield**factor))
    homeRealProb = real_ratio - effortEffectHome + effortEffectAway
    awayRealProb = 1 - real_ratio - effortEffectAway + effortEffectHome

    # Create a normal chance.
    chanceType = mddCreateNormalChance(shared, exclusiveHome, exclusiveAway)

    # Shared chance!
    if chanceType == 1:
      shared += 1
      effortIncreasing = effortIncreasingCreateSharedChance
      if mddHomeWon(homeRealProb):
        homeChances += 1
        effortLevelHome += effortIncreasing * (1 - homeRealProb)
      else:
        awayChances += 1
        effortLevelAway += effortIncreasing * (1 - awayRealProb)
    else:
      # Exclusive home!
      if chanceType == 2:
        exclusiveHome += 1
        if mddHomeWon(homeRealProb):
          homeChances += 1
          effortIncreasing = effortIncreasingCreateExclusiveChance
          effortLevelHome += effortIncreasing * (1 - homeRealProb)
        else:
          effortIncreasing = effortIncreasingDestroyExclusiveChance
          effortLevelAway += effortIncreasing * (1 - awayRealProb)
      # Exclusive Away!
      else:
        exclusiveAway += 1
        if mddHomeWon(homeRealProb):
          effortIncreasing = effortIncreasingDestroyExclusiveChance
          effortLevelHome += effortIncreasing * (1 - homeRealProb)
        else:
          awayChances += 1
          effortIncreasing = effortIncreasingCreateExclusiveChance
          effortLevelAway += effortIncreasing * (1 - awayRealProb)

  return [homeChances, awayChances, homeChances - awayChances]


# frequency function
def CountFrequency(my_list): 
  freq = {}
  for item in my_list: 
    if (item in freq): 
      freq[item] += 1
    else: 
      freq[item] = 1
  for key, value in sorted(freq.items()): 
    print ("% d : % d"%(key, value)) 





# run the chosen simulations
def make_simtypes():
  simCount, currentME, staticNorm, randRange, useEffort = get_params()
  print("\npreparing", nrOfSimulations, "simulations...")
  if currentME:
    print("simulating current ME...")
    normalsimulation = [ runMatch() for x in range(simCount) ]
    simtypes["IndependentME"] = normalsimulation
  if staticNorm:
    print("simulating static modifier...")
    staticsimulation = [ runMatchWithMemory() for x in range(simCount) ]
    simtypes["staticMemoryMod"] = staticsimulation
  if randRange:
    print("simulating modified randomRange...")
    randomrangesimulation = [ runMatchWithRandomRangeMemory() for x in range(simCount) ]
    simtypes["randomRangeMod"] = randomrangesimulation
  if useEffort:
    print("simulating dynamic effort...")
    effortsimulation = [ runMatchWithEffort() for x in range(simCount) ]
    simtypes["dynamicEffort"] = effortsimulation

def plot_chance_stats():
  for i, sector in enumerate(sectors):
    for j, simtype in enumerate(simtypes):
      pltdata = list(zip(*simtypes[simtype]))[i]
      print(pltdata)
      sns.displot(data={simtype:list(zip(*simtypes[simtype]))[i]}, kind="kde")

def plot_chance_stats_new():
  resultArray = []
  for i, sector in enumerate(sectors):
    resultArray.append(dict())
    for simtype in simtypes:
      resultArray[i][simtype] = list(zip(*simtypes[simtype]))[i]
    sns.displot(data=resultArray[i], kind="kde")

# test phase
def init_debug_values():
  simCount = 10000
  currentME = True
  staticNorm = True
  randRange = True
  useEffort = True
  return simCount, currentME, staticNorm, randRange, useEffort

def get_params():
  simCount, currentME, staticNorm, randRange, useEffort = init_debug_values()
  if live: return nrOfSimulations, do_normal, static_normal, static_random, do_effort
  else: return simCount, currentME, staticNorm, randRange, useEffort

def get_debug_vars():
  home_midfield = 10000 #10000
  away_midfield = 10000  #7900
  factor = 3.0
  return home_midfield, away_midfield, factor

def do_debug():
  make_simtypes()
#  plot_chance_stats()
  plot_chance_stats_new()
  print_chance_stats()


def print_chance_stats():
  sectors[2] = "DIFF, HOME"
  for i, sector in enumerate(sectors):
    print("\n**********", sector, "CHANCES ***************************")
    for j, simtype in enumerate(simtypes):
      print()
      print(simtype,'mean:\t\t\t', statistics.mean(list(zip(*simtypes[simtype]))[i]))
#      print(simtype,'variance:\t\t', statistics.variance(list(zip(*simtypes[simtype]))[i]))
      print(simtype,'standard deviation:\t', statistics.stdev(list(zip(*simtypes[simtype]))[i]))
      CountFrequency(list(zip(*simtypes[simtype]))[i])
  if show: plt.show()


live = False
show = False

# main
if __name__ == '__main__':
  scriptstart = datetime.datetime.now().time()
  print("\nscript started:\t",scriptstart,"\n")

  if live:
    make_simtypes()
    plot_chance_stats_new()
    print_chance_stats()
  else: do_debug()

  scriptend = datetime.datetime.now().time()
  print("\nscript finished:\t",scriptend)
  time_elapsed = ( scriptend.second - scriptstart.second ) + ( ( scriptend.minute - scriptstart.minute ) * 60 ) + ( ( scriptend.hour - scriptstart.hour ) * 3600 )
  print("\ntime elapsed:\t",time_elapsed," seconds")

